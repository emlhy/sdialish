package com.emlhy.sdialish.Fragments

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.gesture.GestureLibrary
import android.graphics.Bitmap
import android.os.AsyncTask
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.widget.SearchView
import com.emlhy.sdialish.Adapters.ContactsAdapter
import com.emlhy.sdialish.Objects.Contact
import com.emlhy.sdialish.Objects.Item
import com.emlhy.sdialish.R
import com.emlhy.sdialish.Views.ColorGenerator
import com.emlhy.sdialish.Views.RoundImageView
import android.support.v4.content.ContextCompat
import com.emlhy.sdialish.Managers.GestureManager
import android.graphics.Canvas
import com.emlhy.sdialish.Objects.Header
import com.emlhy.sdialish.Views.CharacterDrawable
import java.util.*
import android.content.ContentResolver
import android.provider.ContactsContract
import android.content.ContentUris
import android.graphics.BitmapFactory
import android.os.Handler
import android.provider.ContactsContract.CommonDataKinds.Phone
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.fragment_contact_list.*
import kotlinx.android.synthetic.main.fragment_contact_list.view.*
import java.lang.ref.WeakReference

class ContactListFragment : Fragment() {
    private val names = ArrayList<String>()
    private val numbers = ArrayList<String>()
    private val photos = ArrayList<Bitmap>()
    private val bitmaps = ArrayList<Bitmap>()
    private val types = ArrayList<String>()
    private val hasGestureContacts = ArrayList<Contact>()
    private val allContacts = ArrayList<Contact>()
    private val filterContacts = ArrayList<Contact>()
    private var contactsAdapter: ContactsAdapter? = null

    private var thumbnailSize: Int = 0
    private var thumbnailInset: Int = 0
    private var pathColor: Int = 0
    lateinit var res: Resources
    lateinit var bitmap: Bitmap
    internal var generator = ColorGenerator.DEFAULT

    private var roundImageView: RoundImageView? = null

    lateinit var contactLinearLayoutManager: LinearLayoutManager

    lateinit var onSwitchFragmentListener: OnSwitchFragmentListener


    companion object {
        fun newInstance(): ContactListFragment {
            return ContactListFragment()
        }
        private var bundleState: Bundle? = null
        val itemList = ArrayList<Item>()
        val searchList = ArrayList<Item>()
        var flag = false
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onSwitchFragmentListener = activity as OnSwitchFragmentListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_contact_list, container, false)


        rootView.toolbar.setTitle(R.string.contact_list)
//        rootView.toolbar.setTitleTextColor(ContextCompat.getColor(activity as Context, android.R.color.white))
//        setHasOptionsMenu(true)
        rootView.toolbar.inflateMenu(R.menu.searchview_menu)
        setupSearchView(rootView.toolbar.menu)

        roundImageView = RoundImageView(activity!!)
        flag = false
        res = resources
        pathColor = ContextCompat.getColor(activity!!, R.color.colorPrimary)
        thumbnailInset = res.getDimension(R.dimen.gesture_thumbnail_inset).toInt()
        thumbnailSize = res.getDimension(R.dimen.gesture_thumbnail_size).toInt()

        rootView.loading_layout.visibility = View.VISIBLE
        if (itemList.size > 0 && contactsAdapter != null) {
            LoadGestures(activity!!.applicationContext, this).execute()
        } else {
            LoadContacts(activity!!.applicationContext, this).execute()
            contactLinearLayoutManager = LinearLayoutManager(activity)
            rootView.contact_list.layoutManager = contactLinearLayoutManager
            contactsAdapter = ContactsAdapter(activity as Context, this, onSwitchFragmentListener)
            contactsAdapter!!.setItemList(itemList)
            rootView.contact_list.adapter = contactsAdapter
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
//        onViewStateRestored(bundleState)
//        if(bundleState != null){
//            val listState: Parcelable? = bundleState!!.getParcelable("KEY_RECYCLER_STATE")
//            if (listState != null)
//                contact_list.layoutManager?.onRestoreInstanceState(listState)
//		}
    }

    override fun onPause() {
        super.onPause()
        bundleState = Bundle()
        onSaveInstanceState(bundleState!!)
//        state = contact_list.layoutManager?.onSaveInstanceState()
//        bundleState = Bundle()
//        bundleState!!.putParcelable("KEY_RECYCLER_STATE", state)
    }

    override fun onDestroy() {
        super.onDestroy()
        searchList.clear()
        itemList.clear()
        contactsAdapter?.notifyDataSetChanged()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        if (savedInstanceState != null){
            val listState: Parcelable? = savedInstanceState.getParcelable("KEY_RECYCLER_STATE")
            Handler().post { contact_list.layoutManager?.onRestoreInstanceState(listState) }
        }
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable("KEY_RECYCLER_STATE", contact_list.layoutManager?.onSaveInstanceState())
        super.onSaveInstanceState(outState)
    }

    class LoadGestures(private val context: Context, private val fragmentContext: ContactListFragment) :  AsyncTask<Void, Void, Void>() {
        private lateinit var contextRef: WeakReference<ContactListFragment>
        private lateinit var fragment: ContactListFragment
        private var gestureLibrary: GestureLibrary? = null

        override fun onPreExecute() {
            contextRef = WeakReference(fragmentContext)
            fragment = contextRef.get()!!
            gestureLibrary = GestureManager.getStore(context)
            fragment.bitmaps.clear()
            itemList.clear()
            fragment.allContacts.clear()
            fragment.hasGestureContacts.clear()
        }

        override fun doInBackground(vararg p0: Void?): Void? {
            for (i in 0 until fragment.numbers.size) {
                val color = fragment.generator.getDefaultColor()
                val drawable = CharacterDrawable(' ', color)
                fragment.bitmap = Bitmap.createBitmap(
                    fragment.thumbnailSize, fragment.thumbnailSize, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(fragment.bitmap)
                drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
                drawable.draw(canvas)
                fragment.bitmaps.add(fragment.bitmap)
            }

            if (gestureLibrary != null) {
                for (name in gestureLibrary!!.gestureEntries) {
                    for (gesture in gestureLibrary!!.getGestures(name)) {
                        fragment.bitmap = gesture.toBitmap(fragment.thumbnailSize, fragment.thumbnailSize, fragment.thumbnailInset, fragment.pathColor)
                        for (i in 0 until fragment.numbers.size) {
                            if (fragment.numbers[i] == name) {
                                fragment.bitmaps[i] = fragment.bitmap
                                fragment.hasGestureContacts.add(
                                    Contact(fragment.photos[i],
                                        fragment.names[i],
                                        fragment.numbers[i],
                                        fragment.types[i], fragment.bitmap)
                                )
                            }
                        }
                    }
                }

                for (i in 0 until fragment.numbers.size) {
                    fragment.allContacts.add(
                        Contact(
                            fragment.photos[i],
                            fragment.names[i],
                            fragment.numbers[i], fragment.types[i], fragment.bitmaps[i]
                        )
                    )
                }

                fragment.sortContacts(fragment.hasGestureContacts)
                fragment.sortContacts(fragment.allContacts)

                if (fragment.hasGestureContacts.size > 0) {
                    itemList.add(
                        Header(R.string.sdailed_contacts_header)
                    )
                    itemList.addAll(fragment.hasGestureContacts)
                }

                itemList.add(
                    Header(R.string.all_contacts_header)
                )
                itemList.addAll(fragment.allContacts)

                //return STATUS_SUCCESS
            }
            return null
            //return STATUS_NOT_LOADED
        }

        override fun onPostExecute(result: Void?) {
            val contactLinearLayoutManager = LinearLayoutManager(context)
            fragment.contact_list.layoutManager = contactLinearLayoutManager
            fragment.contactsAdapter = ContactsAdapter(context, fragment, fragment.onSwitchFragmentListener)
            fragment.contactsAdapter!!.setItemList(itemList)
            fragment.contact_list.adapter = fragment.contactsAdapter
            fragment.loading_layout.visibility = View.GONE
            if(bundleState != null){
                val listState: Parcelable? = bundleState!!.getParcelable("KEY_RECYCLER_STATE")
                if (listState != null)
                    Handler().post { fragment.contact_list.layoutManager?.onRestoreInstanceState(listState) }
            }
//			if (fragment.state != null){
//				fragment.contactLinearLayoutManager.onRestoreInstanceState(fragment.state)
//			}
        }

    }

    class LoadContacts(private val context: Context, private val fragmentContext: ContactListFragment) :  AsyncTask<Void, Void, Void>() {
        private lateinit var contextRef: WeakReference<ContactListFragment>
        private lateinit var fragment: ContactListFragment
        private var resolver: ContentResolver? = null

        override fun onPreExecute() {
            contextRef = WeakReference(fragmentContext)
            fragment = contextRef.get()!!
            resolver = context.contentResolver
        }

        override fun doInBackground(vararg p0: Void?): Void? {
            val phoneCursor = resolver!!.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null
            )
            if (phoneCursor != null && phoneCursor.count > 0) {
                val idColumn = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)
                val displayNameColumn = phoneCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
                while (phoneCursor.moveToNext()) {
                    // retrieve contact ID
                    val contactId = phoneCursor.getString(idColumn)
                    // retrieve contact name
                    val contactName = phoneCursor.getString(displayNameColumn)
                    // return 0 if contact do not have a phone number
                    val phoneCount = phoneCursor
                        .getInt(phoneCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))

                    val uri = ContentUris.withAppendedId(
                        ContactsContract.Contacts.CONTENT_URI,
                        java.lang.Long.parseLong(contactId)
                    )
                    val input = ContactsContract.Contacts
                        .openContactPhotoInputStream(resolver, uri)
                    var contactPhoto: Bitmap
                    if (input != null) {
                        contactPhoto = BitmapFactory.decodeStream(input)
                        contactPhoto = Bitmap.createScaledBitmap(contactPhoto,
                            fragment.thumbnailSize, fragment.thumbnailSize, true)
                    } else {
                        val color = fragment.generator.getColor(contactName)
                        val drawable = CharacterDrawable(contactName.toUpperCase(Locale.ENGLISH)[0], color)
                        contactPhoto = Bitmap.createBitmap(fragment.thumbnailSize, fragment.thumbnailSize, Bitmap.Config.ARGB_8888)
                        val canvas = Canvas(contactPhoto)
                        drawable.setBounds(0, 0, canvas.width, canvas.height)
                        drawable.draw(canvas)
                    }

                    if (phoneCount > 0) {
                        // retrieve contact numbers
						val phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = " + contactId, null, null)
                        if (phones != null && phones.count > 0) {
                            var nameExist = false
                            for (i in 0 until fragment.names.size) {
                                if (fragment.names[i] == contactName) {
                                    nameExist = true
                                    break
                                }
                            }
                            if (nameExist) {
                                continue
                            } else {
                                var type = ""
                                while (phones.moveToNext()) {
                                    // traverse all numbers
                                    val phoneNumber = phones
                                        .getString(
                                            phones
                                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                                        )
                                    val phoneType = phones
                                        .getInt(
                                            phoneCursor
                                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE)
                                        )
                                    when (phoneType) {
                                        Phone.TYPE_MOBILE -> type = "Mobile"
                                        Phone.TYPE_HOME -> type = "Home"
                                        Phone.TYPE_WORK -> type = "Work"
                                        Phone.TYPE_OTHER -> type = "Other"
                                    }

                                    fragment.names.add(contactName)
                                    fragment.numbers.add(phoneNumber)
                                    fragment.photos.add(contactPhoto)
                                    fragment.types.add(type)
                                }
                                phones.close()
                            }
                        }
                        //phones.close()
                    }
                }
                phoneCursor.close()
            }
//                phoneCursor.close()
            return null
        }

        override fun onPostExecute(result: Void?) {
            for(i in 0 until fragment.photos.size){
                fragment.photos.set(i, fragment.roundImageView!!.getCroppedBitmap(
                    fragment.photos[i], 160))
			}
            LoadGestures(context, fragment).execute()
        }
    }

    private fun sortContacts(contacts: ArrayList<Contact>) {
        Collections.sort(contacts, object : Comparator<Contact> {
            override fun compare(lhs: Contact, rhs: Contact): Int {
                return lhs.contactName.toLowerCase(Locale.ENGLISH)
                    .compareTo(rhs.contactName.toLowerCase(Locale.ENGLISH))
            }
        })
    }

    fun filter(query: String) {
		val q = query.toLowerCase(Locale.ENGLISH)
		filterContacts.clear()
		if (q.isEmpty()) {
			filterContacts.addAll(allContacts)
		} else {
			for (i in 0 until allContacts.size) {
				val contact = allContacts[i]
				if (contact.contactName.toLowerCase(Locale.ENGLISH)
						.contains(q)
						|| contact.contactName.toLowerCase().contains(q)) {
					filterContacts.add(contact)
				}
			}
		}
		searchList.add(Header(R.string.search_result))
		searchList.addAll(filterContacts)
        contactsAdapter!!.setItemList(searchList)
        contact_list.adapter?.notifyDataSetChanged()
	}

    private fun setupSearchView(menu: Menu){
        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem?.actionView as SearchView
        searchView.isIconified = false
        searchView.isSubmitButtonEnabled = false
        searchView.setIconifiedByDefault(false)
//        searchView.isFocusable = false
//        searchView.clearFocus()

        val inputMethodManager = activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

//        searchItem.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener{
//            override fun onMenuItemClick(p0: MenuItem?): Boolean {
//                searchView.onActionViewExpanded()
//                return true
//            }
//
//        })

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                flag = true
                searchList.clear()
                filter(p0!!)
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if (p0.equals("")) {
                    searchList.clear()
                    contactsAdapter!!.setItemList(searchList)
                    contact_list.adapter?.notifyDataSetChanged()
                } else {
                    flag = true
                    searchList.clear()
                    filter(p0!!)
                }
                return false
            }

        })

        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener{
            override fun onMenuItemActionExpand(p0: MenuItem?): Boolean {
                searchView.requestFocus()
                inputMethodManager.showSoftInput(searchView.findViewById(R.id.search_src_text), 0)
                return true
            }

            override fun onMenuItemActionCollapse(p0: MenuItem?): Boolean {
                searchView.clearFocus()
//                if (searchList.size == 0) {
                    flag = false
                    contactsAdapter!!.setItemList(itemList)
                    contact_list.adapter?.notifyDataSetChanged()
//                } else {
//                    flag = true
//                    contactsAdapter!!.setItemList(searchList)
//                    contact_list.adapter?.notifyDataSetChanged()
//                }
                return true
            }

        })

        searchView.setOnQueryTextFocusChangeListener { p0, p1 ->
            if (p1) {
                inputMethodManager.showSoftInput(p0, 0)
            } else {
                inputMethodManager.hideSoftInputFromWindow(p0?.windowToken, 0)
            }
        }
    }

    interface OnSwitchFragmentListener {
        fun switchToCreateGesture(number: String, name: String, type: String)
    }
}