package com.emlhy.sdialish.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import com.emlhy.sdialish.R
import com.emlhy.sdialish.Objects.NamedGesture
import android.gesture.Gesture
import android.gesture.GestureOverlayView
import android.view.*
import com.emlhy.sdialish.Activities.GestureManagementActivity.Companion.CONTACT_NAME
import com.emlhy.sdialish.Activities.GestureManagementActivity.Companion.NUMBER_TYPE
import com.emlhy.sdialish.Activities.GestureManagementActivity.Companion.PHONE_NUMBER
import kotlinx.android.synthetic.main.fragment_create_gesture.view.*
import com.emlhy.sdialish.Managers.GestureManager
class CreateGestureFragment : Fragment() {

    val SAVED_CONTACT_NUMBER = "SAVED_CONTACT_NUMBER"
    val SAVED_CONTACT_NAME = "SAVED_CONTACT_NAME"
    val SAVED_CONTACT_TYPE = "SAVED_CONTACT_TYPE"

    private val LENGTH_THRESHOLD = 120.0f

    private var gesture: Gesture? = null
    private var phoneNumber: String? = null
    private var contactName: String? = null
    private var numberType: String? = null
    private lateinit var menu: Menu
    private var namedGesture = NamedGesture()

    companion object {
        fun newInstance(): CreateGestureFragment {
            return CreateGestureFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_create_gesture, container, false)

        val resource = activity!!.resources
        val bundle = arguments
        phoneNumber = bundle?.getString(PHONE_NUMBER)
        contactName = bundle?.getString(CONTACT_NAME)
        numberType = bundle?.getString(NUMBER_TYPE)

        if (savedInstanceState != null) {
            phoneNumber = savedInstanceState.getString(SAVED_CONTACT_NUMBER)
            contactName = savedInstanceState.getString(SAVED_CONTACT_NAME)
            numberType = savedInstanceState.getString(SAVED_CONTACT_TYPE)
        }

        rootView.toolbar.title = contactName
        rootView.toolbar.subtitle = numberType
        rootView.toolbar.inflateMenu(R.menu.create_gesture_menu)
        menu = rootView.toolbar.menu
        val checkMark = menu.findItem(R.id.confirm_create_gesture)
        checkMark.setOnMenuItemClickListener {
            addGesture()
            return@setOnMenuItemClickListener true
        }

        val gestureLib = GestureManager.getStore(activity!!.applicationContext)
        if (gestureLib != null && phoneNumber !=  null) {
            for (name in gestureLib.gestureEntries) {
                for (gesture in gestureLib.getGestures(name)) {
                    if (name == phoneNumber) {
                        namedGesture.name = phoneNumber!!
                        namedGesture.gesture = gesture
                    }
                }
            }
        }

        rootView.gesture_create_overlay.addOnGestureListener(object : GestureOverlayView.OnGestureListener{
            override fun onGestureStarted(p0: GestureOverlayView?, p1: MotionEvent?) {
                setAcceptIconEnabled(false)
                gesture = null
            }

            override fun onGestureCancelled(p0: GestureOverlayView?, p1: MotionEvent?) {

            }

            override fun onGesture(p0: GestureOverlayView?, p1: MotionEvent?) {

            }

            override fun onGestureEnded(p0: GestureOverlayView?, p1: MotionEvent?) {
                gesture = p0?.gesture
                if (gesture != null) {
                    if (gesture!!.length < LENGTH_THRESHOLD) {
                        p0?.clear(false)
                        setAcceptIconEnabled(false)
                    } else {
                        setAcceptIconEnabled(true)
                    }
                }
            }

        })
        rootView.gesture_create_overlay.fadeOffset = resource.getInteger(R.integer.gesture_fade_offset).toLong()

        return rootView
    }

    private fun addGesture() {
        val gestureLibrary = GestureManager.getStore(activity!!.applicationContext)
        if (gestureLibrary != null) {
            gestureLibrary.removeGesture(namedGesture.name, namedGesture.gesture)
            gestureLibrary.save()
            gestureLibrary.addGesture(phoneNumber, gesture)
            gestureLibrary.save()
        }
        activity!!.supportFragmentManager.popBackStack()
    }

    private fun setAcceptIconEnabled(enable: Boolean) {
        val item = menu.findItem(R.id.confirm_create_gesture)
        if (enable) {
            item.setIcon(R.drawable.ic_check)
        } else {
            item.setIcon(R.drawable.ic_check_gray)
        }
        item.setEnabled(enable)
    }
}