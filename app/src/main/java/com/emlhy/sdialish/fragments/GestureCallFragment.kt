package com.emlhy.sdialish.Fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.emlhy.sdialish.R
import android.widget.Toast
import android.gesture.Prediction
import com.emlhy.sdialish.Activities.GestureManagementActivity
import com.emlhy.sdialish.Managers.CallManager
import com.emlhy.sdialish.Managers.GestureManager
import kotlinx.android.synthetic.main.fragment_gesture_call.view.*

class GestureCallFragment : Fragment() {

    companion object {
        fun newInstance(): GestureCallFragment {
            return GestureCallFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_gesture_call, container, false)

        val callManager = CallManager(activity as Context)

        rootView.gesture_call_overlay.setFadeOffset(activity!!.resources.getInteger(R.integer.gesture_fade_offset).toLong())

        val gestureLibrary = GestureManager.getStore(activity!!.applicationContext)

        rootView.contact_btn.setOnClickListener{
            val intent = Intent(activity, GestureManagementActivity::class.java)
            startActivity(intent)
        }

        rootView.gesture_call_overlay.addOnGesturePerformedListener { overlay, gesture ->
            if (gestureLibrary != null) {
                val predictions = gestureLibrary.recognize(gesture)
                if (predictions.size > 0) {
                    val prediction = predictions.get(0) as Prediction
                    if (prediction.score > 1.0) {
                        callManager.startCall(prediction.name)
                        activity!!.finish()
                    } else {
                        Toast.makeText(activity, getString(R.string.no_matched_gesture), Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.no_gesture_set), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(activity, getString(R.string.no_gesture_set), Toast.LENGTH_SHORT).show()
            }
        }
        return rootView
    }
}