package com.emlhy.sdialish.Objects

import android.graphics.Bitmap
import com.emlhy.sdialish.Adapters.ContactsAdapter

class Contact(val contactAvatar: Bitmap, val contactName: String, val phoneNumber: String, val numberType: String, var gesture: Bitmap) : Item {
    override fun getViewType(): Int {
        return ContactsAdapter.RowType.ROW_ITEM.ordinal
    }
}