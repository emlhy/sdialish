package com.emlhy.sdialish.Objects

import com.emlhy.sdialish.Adapters.ContactsAdapter

class Header(val headerTextResource: Int) : Item {
    override fun getViewType(): Int {
        return ContactsAdapter.RowType.HEADER_ITEM.ordinal
    }
}