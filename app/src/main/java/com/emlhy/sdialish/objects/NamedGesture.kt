package com.emlhy.sdialish.Objects

import android.gesture.Gesture

class NamedGesture {
    var name: String? = null
    var gesture: Gesture? = null
}