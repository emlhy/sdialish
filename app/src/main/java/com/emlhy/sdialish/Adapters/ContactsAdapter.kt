package com.emlhy.sdialish.Adapters

import android.content.Context
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.emlhy.sdialish.Fragments.ContactListFragment
import com.emlhy.sdialish.Fragments.ContactListFragment.Companion.flag
import com.emlhy.sdialish.Fragments.ContactListFragment.Companion.itemList
import com.emlhy.sdialish.Fragments.ContactListFragment.Companion.searchList
import com.emlhy.sdialish.Managers.GestureManager
import com.emlhy.sdialish.Objects.Contact
import com.emlhy.sdialish.Objects.Header
import com.emlhy.sdialish.Objects.Item
import com.emlhy.sdialish.Objects.NamedGesture
import com.emlhy.sdialish.R

class ContactsAdapter(private val context: Context, private val fragment: ContactListFragment, private val onSwitchFragmentListener: ContactListFragment.OnSwitchFragmentListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val itemList = ArrayList<Item>()
    enum class RowType {
        ROW_ITEM, HEADER_ITEM
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        if (p1 == RowType.ROW_ITEM.ordinal) {
            val view = LayoutInflater.from(p0.context).inflate(R.layout.item_contacts, p0, false)
            return ContactViewHolder(view, context)
        } else {
            val view = LayoutInflater.from(p0.context).inflate(R.layout.item_header, p0, false)
            return HeaderViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p0 is ContactViewHolder) {
            p0.contactAvatar.setImageBitmap((itemList[p1] as Contact).contactAvatar)
            p0.contactName.text = (itemList[p1] as Contact).contactName
            p0.numberTyper.text = (itemList[p1] as Contact).numberType
            p0.phoneNumber.text = (itemList[p1] as Contact).phoneNumber
            p0.gesture.setImageBitmap((itemList[p1] as Contact).gesture)
            p0.itemView.setOnClickListener {
                if (!flag) {
                    if (itemList[p1] is Contact) {
                        val item = itemList[p1] as Contact
                        onSwitchFragmentListener.switchToCreateGesture(item.phoneNumber, item.contactName, item.numberType)
                    }
                } else {
                    if (searchList[p1] is Contact) {
                        val item = searchList[p1] as Contact
                        onSwitchFragmentListener.switchToCreateGesture(item.phoneNumber, item.contactName, item.numberType)
                    }
                }
            }
            p0.itemView.setOnLongClickListener {
                val item = itemList[p1]
                if (item is Contact) {
                    val contact = item as Contact
                    val number = contact.phoneNumber
                    val gestureLibrary = GestureManager.getStore(context.applicationContext)
                    val namedGesture = NamedGesture()
                    if (gestureLibrary?.getGestures(number) != null) {
                        gestureLibrary.gestureEntries.forEach { name ->
                            gestureLibrary.getGestures(name).forEach { gesture ->
                                if (name == number) {
                                    namedGesture.name = number
                                    namedGesture.gesture = gesture
                                }
                            }
                        }
                        val builder = AlertDialog.Builder(fragment.activity as Context)
                        builder.setTitle("Delete gesture " + contact.contactName + "-" + contact.numberType)
                        builder.setMessage("Are you sure?")
                        builder.setCancelable(true)
                        builder.setPositiveButton("YES") { dialog, which ->
                            deleteGesture(namedGesture)
                        }
                        builder.setNegativeButton("No") { dialog, which -> }
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    }
                }
                return@setOnLongClickListener true
            }
        } else if (p0 is HeaderViewHolder) {
            p0.headerText.setText((itemList[p1] as Header).headerTextResource)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return itemList[position].getViewType()
    }

    class ContactViewHolder(itemView: View, private val context: Context) : RecyclerView.ViewHolder(itemView){
        var contactAvatar: ImageView = itemView.findViewById(R.id.contact_avatar)
        var contactName: TextView = itemView.findViewById(R.id.contact_name)
        var numberTyper: TextView = itemView.findViewById(R.id.number_type)
        var phoneNumber: TextView = itemView.findViewById(R.id.phone_number)
        var gesture: ImageView = itemView.findViewById(R.id.gesture)
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var headerText: TextView = itemView.findViewById(R.id.header_text)
    }

    fun setItemList(itemList: ArrayList<Item>){
        this.itemList.clear()
        this.itemList.addAll(itemList)
    }

    private fun deleteGesture(namedGesture: NamedGesture ) {
        val gestureLib = GestureManager.getStore(context.applicationContext)
        if (gestureLib != null) {
            gestureLib.removeGesture(namedGesture.name, namedGesture.gesture)
            gestureLib.save()

            Toast.makeText(context, R.string.gesture_deleted,
                Toast.LENGTH_SHORT).show()
        }
        ContactListFragment.LoadGestures(context, fragment).execute()
    }
}