package com.emlhy.sdialish.Managers

import android.gesture.GestureLibraries
import android.content.Context
import android.gesture.GestureLibrary
import com.emlhy.sdialish.R
import java.io.File

class GestureManager{
    companion object {
        var gestureLibrary : GestureLibrary? = null
        fun getStore(context: Context): GestureLibrary? {
            if (gestureLibrary == null) {
                val storeFile = File(context.filesDir, context.getResources().getString(R.string.gesture_file_name))
                gestureLibrary = GestureLibraries.fromFile(storeFile)
                if (gestureLibrary?.load() == false) {
                    return null
                }
            }
            return gestureLibrary
        }
    }
}