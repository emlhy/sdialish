package com.emlhy.sdialish.Managers

import android.content.Context
import android.content.Intent
import android.net.Uri

class CallManager (private val context: Context){

    fun startCall(url: String) {
        var url = url
        if (!url.startsWith("tel:")) {
            url = "tel:$url"
        }
        val intent = Intent(Intent.ACTION_CALL, Uri.parse(url))
        context.startActivity(intent)
    }
}