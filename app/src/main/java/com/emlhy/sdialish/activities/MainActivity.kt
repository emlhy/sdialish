package com.emlhy.sdialish.Activities

import android.Manifest
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.emlhy.sdialish.R
import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.widget.Toast
import com.emlhy.sdialish.Fragments.GestureCallFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val REQUEST_CODE_PERMISSIONS = 415

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermission()
        if (fragment_container != null) {
            if (savedInstanceState == null) {
                val gestureCallFragment = GestureCallFragment.newInstance()
                supportFragmentManager.beginTransaction().add(R.id.fragment_container, gestureCallFragment).commit()
            }
        }
    }

    fun checkPermission(){
        val permissionsNeeded = ArrayList<String>()

        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, Manifest.permission.CALL_PHONE)) {
            permissionsNeeded.add("Make phone calls")
        }
        if (!addPermission(permissionsList, Manifest.permission.READ_CONTACTS)) {
            permissionsNeeded.add("Load contacts")
        }

        if (permissionsList.size > 0) {
            /*if (permissionsNeeded.size() > 0) {
				// Need Rationale
				String message = "You need to grant access to " + permissionsNeeded.get(0);
				for (int i = 1; i < permissionsNeeded.size(); i++)
					message = message + ", " + permissionsNeeded.get(i);
				showMessageOKCancel(message,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								ActivityCompat.requestPermissions(MainActivity.this, permissionsList.toArray(new String[permissionsList.size()]),
										REQUEST_CODE_ASK_PERMISSIONS);
							}
						});
				return;
			}*/
            ActivityCompat.requestPermissions(
                this, permissionsList.toTypedArray(),
                REQUEST_CODE_PERMISSIONS
            )
            return
        }
    }

    fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission)
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_PERMISSIONS -> {
                val perms = HashMap<String, Int>()
                // Initial
                perms[Manifest.permission.CALL_PHONE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.READ_CONTACTS] = PackageManager.PERMISSION_GRANTED
                // Fill with results
                for (i in permissions.indices)
                    perms[permissions[i]] = grantResults[i]
                // Check for ACCESS_FINE_LOCATION
                if (perms[Manifest.permission.CALL_PHONE] == PackageManager.PERMISSION_GRANTED && perms[Manifest.permission.READ_CONTACTS] == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                } else {
                    // Permission Denied
                    Toast.makeText(this@MainActivity, "Some permissions are denied", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}
