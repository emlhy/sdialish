package com.emlhy.sdialish.Activities

import android.os.Bundle
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import com.emlhy.sdialish.Fragments.ContactListFragment
import com.emlhy.sdialish.Fragments.CreateGestureFragment
import com.emlhy.sdialish.R
import kotlinx.android.synthetic.main.activity_gesture_management.*

class GestureManagementActivity : AppCompatActivity(), ContactListFragment.OnSwitchFragmentListener {
    companion object {
        val PHONE_NUMBER = "PHONE_NUMBER"
        val CONTACT_NAME = "CONTACT_NAME"
        val NUMBER_TYPE = "NUMBER_TYPE"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gesture_management)
        if (fragment_container != null) {
            if (savedInstanceState == null) {
                val contactListFragment = ContactListFragment.newInstance()
                supportFragmentManager.beginTransaction().add(R.id.fragment_container, contactListFragment).commit()
            }
        }
    }

    override fun switchToCreateGesture(number: String, name: String, type: String) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val bundle = Bundle()
        bundle.putString(PHONE_NUMBER, number)
        bundle.putString(CONTACT_NAME, name)
        bundle.putString(NUMBER_TYPE, type)
        val createGestureFragment = CreateGestureFragment()
        createGestureFragment.arguments = bundle
        fragmentTransaction.replace(R.id.fragment_container, createGestureFragment)
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }
}