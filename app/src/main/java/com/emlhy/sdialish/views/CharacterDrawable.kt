package com.emlhy.sdialish.Views

import android.graphics.*
import android.graphics.drawable.ColorDrawable

class CharacterDrawable(private val character: Char, color: Int) : ColorDrawable(color) {
    private lateinit var textPaint: Paint
    private lateinit var borderPaint: Paint
    private val STROKE_WIDTH = 10
    private val SHADE_FACTOR = 0.9f

    init {
        textPaint = Paint()
        borderPaint = Paint()
        // text paint settings
        textPaint.color = Color.WHITE
        textPaint.isAntiAlias = true
        textPaint.isFakeBoldText = true
        textPaint.style = Paint.Style.FILL
        textPaint.textAlign = Paint.Align.CENTER

        // border paint settings
        borderPaint.color = getDarkerShade(color)
        borderPaint.style = Paint.Style.STROKE
        borderPaint.strokeWidth = STROKE_WIDTH.toFloat()
    }

    private fun getDarkerShade(color: Int): Int {
        return Color.rgb(
            (SHADE_FACTOR * Color.red(color)).toInt(),
            (SHADE_FACTOR * Color.green(color)).toInt(),
            (SHADE_FACTOR * Color.blue(color)).toInt()
        )
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        // draw border
        //canvas.drawRect(getBounds(), borderPaint);

        // draw text
        val width = canvas.width
        val height = canvas.height
        textPaint.textSize = (height / 2).toFloat()
        canvas.drawText(
            character.toString(),
            (width / 2).toFloat(),
            height / 2 - (textPaint.descent() + textPaint.ascent()) / 2,
            textPaint
        )
    }

    override fun setAlpha(alpha: Int) {
        textPaint.alpha = alpha
    }

    override fun setColorFilter(cf: ColorFilter?) {
        textPaint.colorFilter = cf
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }
}