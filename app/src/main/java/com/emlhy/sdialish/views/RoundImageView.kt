package com.emlhy.sdialish.Views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.widget.ImageView
import android.graphics.drawable.BitmapDrawable

class RoundImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ImageView(context, attrs, defStyleAttr) {

    override fun onDraw(canvas: Canvas) {
        val drawable = drawable ?: return

        if (width == 0 || height == 0) {
            return
        }
        val b = (drawable as BitmapDrawable).bitmap
        val bitmap = b.copy(Bitmap.Config.ARGB_8888, true)

        val w = width
        val h = height


        val roundBitmap = getCroppedBitmap(bitmap, w)
        canvas.drawBitmap(roundBitmap, 0f, 0f, null)
    }

    fun getCroppedBitmap(bmp: Bitmap, radius: Int): Bitmap {
        val sbmp: Bitmap
        if (bmp.width != radius || bmp.height != radius)
            sbmp = Bitmap.createScaledBitmap(bmp, radius, radius, false)
        else
            sbmp = bmp
        val output = Bitmap.createBitmap(
            sbmp.width,
            sbmp.height, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(output)

        val color = -0x5e688c
        val paint = Paint()
        val rect = Rect(0, 0, sbmp.width, sbmp.height)

        paint.setAntiAlias(true)
        paint.setFilterBitmap(true)
        paint.setDither(true)
        canvas.drawARGB(0, 0, 0, 0)
        paint.setColor(Color.parseColor("#BAB399"))
        canvas.drawCircle(
            sbmp.width / 2 + 0.7f, sbmp.height / 2 + 0.7f,
            sbmp.width / 2 + 0.1f, paint
        )
        paint.setXfermode(PorterDuffXfermode(PorterDuff.Mode.SRC_IN))
        canvas.drawBitmap(sbmp, rect, rect, paint)
        return output
    }
}