package com.emlhy.sdialish.Views

import java.util.*

class ColorGenerator(private val colorList: List<Int>){

    companion object {
        val DEFAULT: ColorGenerator = create(
            Arrays.asList(
                0xfff16364.toInt(),
                0xfff58559.toInt(),
                0xfff9a43e.toInt(),
                0xff8bc34a.toInt(),
                0xff009688.toInt(),
                0xff00bcd4.toInt(),
                0xff2093cd.toInt(),
                0xffad62a7.toInt(),
                0xff607d8b.toInt()
            )
        )

        private fun create(colorList: List<Int>): ColorGenerator {
            return ColorGenerator(colorList)
        }
    }


    private var mColors: List<Int>
    private var mRandom: Random

    init {
        mColors = colorList
        mRandom = Random(System.currentTimeMillis())
    }

    fun getRandomColor(): Int {
        return mColors[mRandom.nextInt(mColors.size)]
    }

    fun getColor(key: Any): Int {
        return mColors[Math.abs(key.hashCode()) % mColors.size]
    }

    fun getDefaultColor(): Int {
        return 0x00000000
    }
}